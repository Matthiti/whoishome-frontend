import axios from 'axios';
import config from '@/config';
import Store from '@/store';
import Router from '@/router';

axios.defaults.baseURL = config.apiUrl;

let interceptor;

if (isLoggedIn()) {
  const token = localStorage.getItem('token');
  axios.defaults.headers.common['X-Authorization'] = 'Bearer ' + token;
  enableInterceptor();
}

function enableInterceptor() {
  interceptor = axios.interceptors.response.use(null, error => {
    if (error.response && error.response.status === 401 && error.config) {
      if (hasRefreshToken()) {
        return refresh().then(response => {
          const token = response.data.token;
          localStorage.setItem('token', token);
          axios.defaults.headers.common['X-Authorization'] = `Bearer ${token}`;
          error.config.headers['X-Authorization'] = `Bearer ${token}`;
          return axios(error.config);
        }).catch(() => {
          logout();
        });
      } else {
        logout();
      }
    }
    return Promise.reject(error);
  });
}

function isLoggedIn() {
  return localStorage.getItem('token') !== null || hasRefreshToken();
}

function hasRefreshToken() {
  return localStorage.getItem('refreshToken') !== null;
}

function setRefreshToken(refreshToken) {
  localStorage.setItem('refreshToken', refreshToken);
}

let refreshTokenRequest;

function refresh() {
  if (!refreshTokenRequest) {
    refreshTokenRequest = axios.post('/v1/auth/refresh', { refresh_token: localStorage.getItem('refreshToken') });
    refreshTokenRequest.then(() => {
      refreshTokenRequest = null;
    }).catch(() => {
      logout();
    });
  }
  return refreshTokenRequest;
}

function afterLogin(token) {
  localStorage.setItem('token', token);
  axios.defaults.headers.common['X-Authorization'] = `Bearer ${token}`;
  enableInterceptor();
}

function logout() {
  localStorage.removeItem('token');
  localStorage.removeItem('refreshToken');
  Store.commit('setUser', null);
  axios.interceptors.response.eject(interceptor);
  Router.push({ name: 'login' });
}

export default {
  isLoggedIn,
  afterLogin,
  logout,
  axios,
  setRefreshToken
};
