import Vue from 'vue';
import VueRouter from 'vue-router';
import Store from '@/store';
import http from '@/services/http';

Vue.use(VueRouter);

const defaultTitle = 'Who Is Home';

const routes = [
  {
    path: '/auth',
    component: () => import(/* webpackChunkName: "auth" */ '../views/layouts/AuthLayout.vue'),
    children: [
      {
        path: 'login',
        name: 'login',
        component: () => import(/* webpackChunkName: "login" */ '../views/auth/Login.vue'),
        meta: {
          title: `Login | ${defaultTitle}`
        }
      },
      {
        path: 'register',
        name: 'register',
        component: () => import(/* webpackChunkName: "register" */ '../views/auth/Register.vue'),
        meta: {
          title: `Register | ${defaultTitle}`
        }
      },
      {
        path: 'forgot-password',
        name: 'forgotPassword',
        component: () => import(/* webpackChunkName: "forgot-password" */ '../views/auth/ForgotPassword.vue')
      },
      {
        path: 'reset-password',
        name: 'resetPassword',
        component: () => import(/* webpackChunkName: "reset-password" */ '../views/auth/ResetPassword.vue')
      }
    ]
  },
  {
    path: '/',
    component: () => import(/* webpackChunkName: "default" */ '../views/layouts/DefaultLayout.vue'),
    meta: {
      requiresAuth: true
    },
    children: [
      {
        path: '',
        name: 'home',
        component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue')
      },
      {
        path: 'history',
        name: 'history',
        component: () => import(/* webpackChunkName: "history" */ '../views/History.vue'),
        meta: {
          requiredFeatures: ['show_history']
        }
      },
      {
        path: 'statistics',
        name: 'statistics',
        component: () => import(/* webpackChunkName: "statistics" */ '../views/Statistics.vue'),
        meta: {
          requiredFeatures: ['show_stats']
        }
      },
      {
        path: 'settings',
        name: 'settings',
        component: () => import(/* webpackChunkName: "settings" */ '../views/Settings.vue')
      },
      {
        path: 'admin',
        component: {
          render: createElement => createElement('router-view')
        },
        meta: {
          title: `Admin | ${defaultTitle}`,
          requiredFeatures: ['show_admin_panel']
        },
        children: [
          {
            path: 'groups',
            name: 'manageGroups',
            component: () => import(/* webpackChunkName: "manage-groups" */ '../views/admin/groups/ManageGroups.vue')
          },
          {
            path: 'groups/add',
            name: 'addGroup',
            component: () => import(/* webpackChunkName: "add-group" */ '../views/admin/groups/AddGroup.vue')
          },
          {
            path: 'groups/:groupId/:groupName/edit',
            name: 'editGroup',
            component: () => import(/* webpackChunkName: "edit-group" */ '../views/admin/groups/EditGroup.vue'),
            props: true
          },
          {
            path: 'people',
            name: 'managePeople',
            component: () => import(/* webpackChunkName: "manage-people" */ '../views/admin/people/ManagePeople.vue')
          },
          {
            path: 'people/add',
            name: 'addPerson',
            component: () => import(/* webpackChunkName: "add-people" */ '../views/admin/people/AddPerson.vue')
          },
          {
            path: 'people/:personId/:personName/edit',
            name: 'editPerson',
            component: () => import(/* webpackChunkName: "edit-person" */ '../views/admin/people/EditPerson.vue'),
            props: true
          },
          {
            path: 'people/:personId/:personName/devices',
            name: 'manageDevices',
            component: () => import(/* webpackChunkName: "manage-devices" */ '../views/admin/devices/ManageDevices.vue'),
            props: true
          },
          {
            path: 'people/:personId/:personName/devices/add',
            name: 'addDevice',
            component: () => import(/* webpackChunkName: "add-device" */ '../views/admin/devices/AddDevice.vue'),
            props: true
          },
          {
            path: 'people/:personId/:personName/devices/:deviceId/:deviceName/edit',
            name: 'editDevice',
            component: () => import(/* webpackChunkName: "edit-device" */ '../views/admin/devices/EditDevice.vue'),
            props: true
          },
          {
            path: 'scanners',
            name: 'manageScanners',
            component: () => import(/* webpackChunkName: "manage-scanners" */ '../views/admin/scanners/ManageScanners.vue')
          },
          {
            path: 'scanners/add',
            name: 'addScanner',
            component: () => import(/* webpackChunkName: "add-scanner" */ '../views/admin/scanners/AddScanner.vue')
          },
          {
            path: 'scanners/:scannerId/:scannerName/edit',
            name: 'editScanner',
            component: () => import(/* webpackChunkName: "edit-scanner" */ '../views/admin/scanners/EditScanner.vue'),
            props: true
          },
          {
            path: 'invite-user',
            name: 'inviteUser',
            component: () => import(/* webpackChunkName: "invite-user" */ '../views/admin/invite-user/InviteUser.vue'),
            meta: {
              requiredFeatures: ['invite_person']
            }
          },
          {
            path: 'auth-tokens',
            name: 'manageAuthTokens',
            component: () => import(/* webpackChunkName: "manage-auth-tokens" */ '../views/admin/auth-tokens/ManageAuthTokens.vue')
          },
          {
            path: 'auth-tokens/add',
            name: 'addAuthToken',
            component: () => import(/* webpackChunkName: "add-auth-token" */ '../views/admin/auth-tokens/AddAuthToken.vue')
          },
          {
            path: 'auth-tokens/:authTokenId/:authTokenName/edit',
            name: 'editAuthToken',
            component: () => import(/* webpackChunkName: "edit-auth-token" */ '../views/admin/auth-tokens/EditAuthToken.vue'),
            props: true
          }
        ]
      }
    ]
  },
  {
    path: '*',
    name: 'notFound',
    component: () => import(/* webpackChunkName: "not-found" */ '../views/NotFound.vue')
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  // Set the title
  document.title = to.matched.slice().reverse().find(route => route.meta.title)?.meta.title || defaultTitle;

  // Check if the route is accessible
  if (!to.matched.some(record => record.meta.requiresAuth)) {
    return next();
  }

  if (!http.isLoggedIn()) {
    return next({ name: 'login' });
  }

  const requiredFeatures = to.matched.flatMap(record => record.meta.requiredFeatures).filter(r => r != null);

  console.log('FEATURES', requiredFeatures);
  const checkAccess = () => {
    for (const feature of requiredFeatures) {
      if (!Store.getters.canI(feature)) {
        return next({ name: 'home' });
      }
    }
    next();
  };

  if (!Store.getters.user) {
    // TODO: maybe move this to somewhere more appropriate?
    Store.dispatch('fetchUser').then(() => {
      checkAccess();
    });
  } else {
    checkAccess();
  }
});

export default router;
