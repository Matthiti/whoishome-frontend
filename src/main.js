import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import vmodal from 'vue-js-modal';
import Notifications from 'vue-notification';
import vSelect from 'vue-select';
import http from '@/services/http';

Vue.config.productionTip = false;

Vue.use(vmodal);
Vue.use(Notifications);

Vue.component('v-select', vSelect);

Vue.prototype.$http = http.axios;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
