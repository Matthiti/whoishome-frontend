import Vue from 'vue';
import Vuex from 'vuex';
import http from '@/services/http';

Vue.use(Vuex);

export default new Vuex.Store({
  getters: {
    user: state => state.user,
    groups: state => state.groups,
    // eslint-disable-next-line eqeqeq
    group: state => groupId => state.groups.find(g => g.id == groupId),
    people: state => state.people,
    peopleInGroup: state => {
      return groupId => {
        // eslint-disable-next-line eqeqeq
        const group = state.groups.find(g => g.id == groupId);
        if (!group) {
          return [];
        }

        return group.person_ids.map(id => state.people.find(p => p.id === id)).filter(p => p);
      };
    },
    // eslint-disable-next-line eqeqeq
    person: state => personId => state.people.find(p => p.id == personId),
    device: state => (personId, deviceId) => {
      // eslint-disable-next-line eqeqeq
      const person = state.people.find(p => p.id == personId);
      if (!person) {
        return null;
      }
      // eslint-disable-next-line eqeqeq
      return person.devices.find(d => d.id == deviceId);
    },
    selectedGroup: state => state.selectedGroup,
    loading: state => state.loading,
    scanners: state => state.scanners,
    // eslint-disable-next-line eqeqeq
    scanner: state => scannerId => state.scanners ? state.scanners.find(s => s.id == scannerId) : null,
    authTokens: state => state.authTokens,
    // eslint-disable-next-line eqeqeq
    authToken: state => authTokenId => state.authTokens ? state.authTokens.find(a => a.id == authTokenId) : null,
    canI: state => feature => {
      if (!state.user) {
        return false;
      }

      for (const f of state.user.role.features) {
        if (f.name === feature) {
          return true;
        }
      }
      return false;
    }
  },
  state: {
    user: null,
    groups: [],
    people: [],
    selectedGroup: null,
    loading: false,
    scanners: null,
    authTokens: null
  },
  mutations: {
    setUser(state, user) {
      state.user = user;
    },
    setGroups(state, groups) {
      state.groups = groups;
    },
    setPeople(state, people) {
      state.people = people;
    },
    setSelectedGroup(state, selectedGroup) {
      state.selectedGroup = selectedGroup;
    },
    setLoading(state, loading) {
      state.loading = loading;
    },
    setScanners(state, scanners) {
      state.scanners = scanners;
    },
    setAuthTokens(state, authTokens) {
      state.authTokens = authTokens;
    }
  },
  actions: {
    fetchUser({ commit }) {
      return new Promise((resolve, reject) => {
        http.axios.get('/v1/me').then(response => {
          commit('setUser', response.data);
          resolve();
        }).catch(error => {
          reject(error);
        });
      });
    },
    fetchGroups({ commit }) {
      return new Promise((resolve, reject) => {
        http.axios.get('/v3/groups').then(response => {
          commit('setGroups', response.data);
          resolve();
        }).catch(error => {
          reject(error);
        });
      });
    },
    fetchPeople({ commit }) {
      return new Promise((resolve, reject) => {
        http.axios.get('/v3/people').then(response => {
          commit('setPeople', response.data);
          resolve();
        }).catch(error => {
          reject(error);
        });
      });
    },
    fetchScanners({ commit }) {
      return new Promise((resolve, reject) => {
        http.axios.get('/v3/scanners').then(response => {
          commit('setScanners', response.data);
          resolve();
        }).catch(error => {
          reject(error);
        });
      });
    },
    fetchAuthTokens({ commit }) {
      return new Promise((resolve, reject) => {
        http.axios.get('/v1/auth/tokens').then(response => {
          commit('setAuthTokens', response.data);
          resolve();
        }).catch(error => {
          reject(error);
        });
      });
    }
  }
});
