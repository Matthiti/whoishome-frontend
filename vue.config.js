module.exports = {
  css: {
    extract: true,
    loaderOptions: {
      sass: {
        prependData: '@import "@/assets/scss/_bootstrap-required.scss";'
      }
    }
  }
};
