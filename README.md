# Who Is Home - Frontend

This is the frontend of the Who Is Home project. Currently, due to security considerations, the backend has not yet been open sourced. I aim to do this in the near future.

## Requirements

- node
- npm

## Installation

```
npm install
```

## Run

```
npm run serve
```

## Build
```
npm run build
```
